import { createStore } from 'redux'
import { StudentReducer } from '../reducers/StudentReducer' 
export const store = createStore(StudentReducer)