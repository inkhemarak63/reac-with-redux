import './App.css';
import Navbar from './components/Navbar'
import Home from './components/Home'
import Student from './components/Student'
import { BrowserRouter, Routes, Route} from "react-router-dom";
function App() {
  return (
    <BrowserRouter>
    <Navbar/>
    <Routes>
      <Route exact path="/" element={<Home/>}></Route>
      <Route path="/student" element={<Student/>}></Route>
    </Routes>
  </BrowserRouter>
  );
}

export default App;
