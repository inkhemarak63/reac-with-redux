const initState ={
    students: []
}
export const StudentReducer = (state = initState, action) =>{
    switch (action.type){
        case 'FETCH_DATA':
            return {...state, students: action.playload.students}
        // case "other action"
        default: 
            return state
    }
}