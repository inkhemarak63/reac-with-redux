import React, { Component } from 'react'
import {Table} from 'react-materialize'
import { connect } from 'react-redux';
import { fetchdata } from '../actions/StudentAction'
class Student extends Component {
  // state = {
  //   student: [
  //     {id: 1, name: "Daleak", sex: "M", position: 'developer'},
  //     {id: 2, name: "Sary", sex: "M", position: 'developer'},
  //     {id: 3, name: "Sydent", sex: "M", position: 'developer'}
  //   ]
  // }
  componentDidMount(){
    this.props.fetchdata();
  }
  render() {
    console.log("arr", this.props.students);
    // const {student}  = this.props.students; //call state
    const student = this.props.students; // call from store
      return (
        <Table>
        <thead>
          <tr>
            <th data-field="id">
              ID
            </th>
            <th data-field="name">
              Name
            </th>
            <th data-field="price">
              Position
            </th>
          </tr>
        </thead>
        <tbody>
          {student && student.map(val => {
            return <tr key={val.id}>
              <td>{val.name}</td>
              <td>Eclair</td>
              <td>$0.87</td>
            </tr>
          })}
        </tbody>
      </Table>
    )
  }
}
const mapStateToProps =(state) =>{
  return {
    students: state.students
  }
}
export default connect(mapStateToProps, {fetchdata}) (Student);